package pages;


import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import pages.basketPage.BasketPO;
import pages.categoriesOrListingPO.ListingPO;
import pages.authorizationPage.AuthorizationPagePO;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;
import pages.mainPage.MainPagePO;

import utils.DriverManager;
import utils.listeners.ScreenshotAndVideoListener;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;


import static utils.DriverManager.*;
import static utils.PropertiesLoader.getProp;

@Listeners({ScreenshotAndVideoListener.class})


public abstract class BaseTest {

    protected MainPagePO mainPage = new MainPagePO();
    protected static AuthorizationPagePO authorizationPage = new AuthorizationPagePO();
    protected ListingPO listing = new ListingPO();
    protected BasketPO basket = new BasketPO();

    protected static StopWatch stopWatch = new StopWatch();



    protected static final String ANSI_YELLOW = "\u001B[33m";//жовтий колір
    protected static final String ANSI_RESET = "\u001B[0m";//білий колір

    @BeforeSuite
    public void beforeSuite() {
        stopWatch.start(); //старт секундоміра
    }

    @BeforeMethod(alwaysRun = true)
    public void BeforeMethod(Method method) {
        getDriver().manage().timeouts().pageLoadTimeout(Long.parseLong(getProp("pageLoadTimeout")), TimeUnit.SECONDS);
        //вивожу перекрашені назви тестів з часом їх запуску
        System.out.println(ANSI_YELLOW + new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + " Старт теста: " + method.getName() +
                " з класа " + this.getClass().getName() + " (" + stopWatch.toString() + ")" + ANSI_RESET);

        Allure.addAttachment("Час запуска теста і час коли тест запустився по секундоміру",
                new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()) + " | " + stopWatch.toString());
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {
        getDriver().manage().deleteAllCookies();

        StringBuilder toString = new StringBuilder();

        //вивід в консоль від конкретний тест прикріплюю під Allure
        List<String> consoleOutput = Reporter.getOutput(result);
        for (String list : consoleOutput) {
            list = list + " \n";
            toString.append(list);
        }

        Allure.addAttachment("Вивід в консоль", toString.toString());

        DriverManager.runScript("window.localStorage.clear();");
    }

    public SessionId session;


    @BeforeTest
    public void beforeTest() {
        acceptSetDriver = true;
        session = ((RemoteWebDriver) getDriver()).getSessionId();
        if (getProp("driverType").equalsIgnoreCase("remote")) {
            getDriver().manage().window().setSize(new Dimension(1920, 1080));

            Point newPoint = new Point(0, 0);
            getDriver().manage().window().setPosition(newPoint);
        }
        if (getProp("browser").equalsIgnoreCase("firefox") & getProp("driverType").equalsIgnoreCase("local")) {
            getDriver().manage().window().maximize();
        }
        System.out.println("ID цієї сесії = " + session);

    }


    @AfterTest
    public void afterTest(ITestContext iTestContext) {
        if (getDriver() != null)
            getDriver().quit();
    }

    @Step("Авторизуємось на сайті з такими даними: {email} і {pass}")
    public static MainPagePO login(String email, String pass) {
        authorizationPage.open();
        authorizationPage.logInUserByPage(email, pass);
        return new MainPagePO();
    }
}

