package elements.basket;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.BaseTest;
import pages.basketPage.elements.BasketProduct;
import pages.categoriesOrListingPO.elements.ListingProduct;

import java.util.ArrayList;

public class BasketPOTests extends BaseTest {

    @Epic(value = "Кошик")
    @Feature(value = "Функціональність кошика")
    @Description("Перевіряємо добавлення в кошик")
    @Test()
    public void basketTest() {
        mainPage.open();
        authorizationPage.logInUserByModal("test.rozetka4321@gmail.com", "12345Qq1");
        mainPage.openRandomListing();

        ListingProduct listProduct = listing.getRandomListingProduct();
        String listingName = listProduct.getName();
        double listingPrice = listProduct.getPrice();
        listProduct.addToBasket();
        Reporter.log(listingName, true);
        Reporter.log(Double.toString(listingPrice), true);

        basket.open();

        ArrayList<BasketProduct> bsktProduct = basket.getProductsList();
        Assert.assertEquals(listingName, bsktProduct.get(0).getName(), "В кошику і лістингу відрізняють назви товару");
        Assert.assertEquals(listingPrice, bsktProduct.get(0).getPrice(), "В кошику і лістингу відрізняють ціни товару");
        Assert.assertEquals(bsktProduct.get(0).getQuantity(), 1.0);
        bsktProduct.get(0).delGoods();

    }
}
