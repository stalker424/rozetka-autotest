package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hacks {
    //отримує double з String
    public static double getDouble(String str) {
        Pattern p = Pattern.compile("(-?[0-9]+(?:[,.]{1}[0-9]+)?)");
        Matcher matcher = p.matcher(str.replaceAll(" ", ""));
        if (matcher.find()) {
            str = matcher.group(1);
        }

        return Double.parseDouble(str);
    }
}
