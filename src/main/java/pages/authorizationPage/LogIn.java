package pages.authorizationPage;


import io.qameta.allure.Step;
import utils.DriverManager;
import org.openqa.selenium.By;
import pages.BasePO;
import pages.mainPage.MainPagePO;


class LogIn extends BasePO {

    LogIn() {
        super(AuthorizationPagePO.getLoginPageUrl());
    }

    private static final By USER_EMAIL_SEL = By.xpath("//input[@id=\"auth_email\"]");
    private static final By USER_PASS_SEL = By.xpath("//input[@id=\"auth_pass\"]");
    private static final By LOGIN_SUBMIT_BTN_SEL = By.xpath("//button[@class=\"button button--large button--green auth-modal__submit\"]");
    private static final By AUTHORIZATION_MODAL_SEL = By.xpath("//button[@class=\"header-topline__user-link button--link\"]");



    @Step("Вводимо пошту")
    public LogIn setEmail(String email) {
        inputData(USER_EMAIL_SEL, email);
        return this;
    }

    @Step("Вводимо пароль")
    public LogIn setPassword(String pass) {
        DriverManager.getDriver().findElement(USER_PASS_SEL).sendKeys(pass);
        return this;
    }

    @Step("Клікаємо Увійти")
    public MainPagePO submit() {
        clickElement(LOGIN_SUBMIT_BTN_SEL);
        return new MainPagePO();
    }

    @Step("Відкриваємо модал авторизації")
    public LogIn openAutorizationModal() {
        clickElement(AUTHORIZATION_MODAL_SEL);
        return new LogIn();
    }

}

