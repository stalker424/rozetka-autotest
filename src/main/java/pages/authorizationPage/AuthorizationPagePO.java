package pages.authorizationPage;

import io.qameta.allure.Step;
import utils.DriverManager;
import pages.BasePO;
import pages.mainPage.MainPagePO;


public class AuthorizationPagePO extends BasePO {

    public LogIn logIn = new LogIn();

    private static final String LOGIN_PAGE_URL = "/signin/";
    public AuthorizationPagePO() { super(LOGIN_PAGE_URL);
    }

    public static String getLoginPageUrl() {
        return LOGIN_PAGE_URL;
    }


    @Step("Авторизовуємось під введеними логіном і паролем на сторінці авторизації")
    public MainPagePO logInUserByPage(String email, String pass) {
        return logIn.setEmail(email).setPassword(pass).submit();
    }
    
    @Step("Авторизовуємось під введеними логіном і паролем ерез модал")
    public MainPagePO logInUserByModal(String email, String pass){
        return logIn.openAutorizationModal().setEmail(email).setPassword(pass).submit();
    }

}
