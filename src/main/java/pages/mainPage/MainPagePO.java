package pages.mainPage;


import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pages.BasePO;
import static utils.DriverManager.getDriver;

import java.util.List;
import java.util.Random;



public class MainPagePO extends BasePO {
    private static final Random RANDOM = new Random();

    private static final String REL_URL = "/";
    public MainPagePO() {
        super(REL_URL);
    }

    private static final By CATALOG_SEL = By.xpath("//span[@class=\"menu-toggler__text\"]");
    private static final By CATEGORY_IN_MAIN_MENU_SEL = By.xpath("//p[@class=\"menu__hidden-title menu__hidden-title_color_gray\"]/../../..");
    private static final By LISTING_IN_MAIN_MENU_SEL = By.xpath("//li[@class=\"menu-categories__item menu-categories__item_state_hovered\"]//a[@class=\"menu__link\"]");
    private static final By ACCEPT_AGE_MODAL = By.xpath("//a[@class=\"btn-link-i exponea-close\"]");

    @Step("Відкриваємо головне меню сайту")
    public void openCatalog() {
        clickElement(CATALOG_SEL);
    }

    @Step("Відкриваємо рандомний лістинг серед рандомних категорій головного меню")
    public void openRandomListing(){
        openCatalog();
        List<WebElement> categoryList = getAllElements(CATEGORY_IN_MAIN_MENU_SEL);
        int i = RANDOM.nextInt(categoryList.size());
        Actions actions = new Actions(getDriver());
        waitUntilElementIsClickable(categoryList.get(i));
        actions.moveToElement(categoryList.get(i)).build().perform();
        List<WebElement> listingList = getAllElements(LISTING_IN_MAIN_MENU_SEL);
        int y = RANDOM.nextInt(listingList.size());
        clickElement(listingList.get(y));
        //на випадок якщо тест зайде в категорію алкоголю
        if(existsElement(ACCEPT_AGE_MODAL)){
            clickElement(ACCEPT_AGE_MODAL);
        }
   }
}

