package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.util.List;
import java.util.concurrent.TimeUnit;


import static utils.DriverManager.*;
import static utils.PropertiesLoader.getProp;

public abstract class BasePO {

    private final String REL_URL;
    private static String BASE_URL;
    private final String LANG_URL;


    public BasePO(String url) {
        REL_URL = url;

        BASE_URL = System.getProperty("baseUrl");

        if (BASE_URL == null) {
            BASE_URL = getProp("baseUrl");
        }

        switch (BASE_URL) {
            case "prod":
                BASE_URL = getProp("prod");
                break;
            default:
                BASE_URL = getProp("prod");
                break;
        }

        LANG_URL = dependsOnLanguage("/" + getProp("language").toLowerCase(), "");
    }

    @Step("Переходимо в: {url}")
    private BasePO open(String url) {
        getDriver().get(url);
        return this;
    }


    public BasePO open() {
        String s = BASE_URL + LANG_URL + REL_URL;
        Reporter.log("Перехід на : " + s, true);
        open(s);
        if (getProp("language").equalsIgnoreCase("ru") & getLang().equals("UA")) {
            changeLang();
        }
        return this;
    }


    public static String getBaseUrl() {
        return BASE_URL;
    }

    //очікування поки елемент стане клікабельним(WebElement)
    public static WebElement waitUntilElementIsClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30, 100);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    //очікування поки елемент стане клікабельним(By)
    public static WebElement waitUntilElementIsClickable(By by) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(by)));
        } catch (StaleElementReferenceException e) {
            Reporter.log("Був StaleElementReferenceException!", true);
            return wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(by)));
        }
    }


    //очікування поки елемент стане видимим(WebElement)
    public static WebElement waitUntilElementIsDisplayed(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }


    //отримати List елементів по конкретному селектору
    public static List<WebElement> getAllElements(By selector) {
        return getDriver().findElements(selector);
    }

    //ввести дані в текстове поле
    public static void inputData(By by, String t) {
        focusedElement(by);
        getDriver().findElement(by).clear();
        focusedElement(by);
        getDriver().findElement(by).sendKeys(t);
    }

    //метод повертає текст по заданому селектору By
    public static String getText(By by) {
        return waitUntilElementIsDisplayed(getDriver()
                .findElement(by))
                .getText();
    }

    //клік на елементі(By)
    public static void clickElement(By by) {
        waitUntilElementIsClickable(by).click();
    }

    //клік на елементі(WebElement)
    public static void clickElement(WebElement element) {
        waitUntilElementIsClickable(element).click();
    }

    //отримати мову сайту
    public static String getLang() {
        return getText(By.xpath("//span[@class=\"header-topline__language-item_state_active\"]"));
    }

    //змінити мову сайта
    protected void changeLang() {
        clickElement(By.xpath("//li[@class=\"header-topline__language-item\"]/a"));
    }


    //отримати атрибут елемента(By)
    public static String getElementAttributeBy(By by, String attribute) {
        return getDriver().findElement(by).getAttribute(attribute);
    }

    //перевести фокус на елемент
    public static void focusedElement(By by) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].focus()", getDriver().findElement(by));
    }

    //метод який присвоює правильні тексти залежно від мови
    public static String dependsOnLanguage(String ukrLang, String rusLang) {
        return getProp("language").equalsIgnoreCase("ua") ? ukrLang : rusLang;
    }

    //якщо елемент існує, то метод повертає true(By)
    public static boolean existsElement(By by) {
        getDriver().manage().timeouts().implicitlyWait(1,
                TimeUnit.SECONDS);
        try {
            getDriver().findElement(by);
            getDriver().manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
        } catch (NoSuchElementException e) {
            getDriver().manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
            return false;
        }
        getDriver().manage().timeouts().implicitlyWait(Long.parseLong(getProp("implicitWait")), TimeUnit.SECONDS);
        return true;
    }
}

