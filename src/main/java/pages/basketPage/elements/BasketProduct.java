package pages.basketPage.elements;

import org.openqa.selenium.By;
import utils.Hacks;

import static pages.BasePO.clickElement;
import static pages.BasePO.getText;
import static utils.DriverManager.getDriver;

public class BasketProduct {

    private static int index;
    private static String PRODUCT_SEL;

    public BasketProduct(int i) {
        index = i;
        PRODUCT_SEL = "//ul[@class=\"cart-list\"]/li["+index+"]";
    }

    private final String NAME_SEL = "//a[@class=\"cart-product__title\"]";
    private final String PRICE_SEL = "//p[@class=\"cart-product__price\"]";
    private final String QUANTITY_SEL = "//input[@class=\"cart-counter__input ng-untouched ng-pristine ng-valid\"]";
    private final String CONTEXT_MENU_SEL = "//button[@id=\"cartProductActions0\"]";
    private final String DEL_GOODS_SEL = "//button[@class=\"button button--medium button--with-icon button--link context-menu-actions__button\"]";

    //отримати назву товару
    public String getName() {
        return getText(By.xpath(PRODUCT_SEL + NAME_SEL));
    }

    //отримати ціну товару
    public double getPrice() {
        return Hacks.getDouble(getText(By.xpath(PRODUCT_SEL + PRICE_SEL)));
    }

    //отримати кількість конкретного товару
    public double getQuantity (){
        return Hacks.getDouble(getDriver().findElement(By.xpath(PRODUCT_SEL + QUANTITY_SEL)).getAttribute("value"));
    }

    //видалямо товару
    public void delGoods(){
        clickElement(By.xpath(PRODUCT_SEL + CONTEXT_MENU_SEL));
        clickElement(By.xpath(PRODUCT_SEL + DEL_GOODS_SEL));
            }
}
