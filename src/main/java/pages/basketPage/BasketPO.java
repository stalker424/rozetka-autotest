package pages.basketPage;

import org.openqa.selenium.By;
import org.testng.Reporter;
import pages.BasePO;
import pages.basketPage.elements.BasketProduct;

import java.util.ArrayList;

public class BasketPO extends BasePO {
    private static final By PRODUCT_ITEM = By.xpath("//ul[@class=\"cart-list\"]/li");

    private static final String REL_URL = "/cart/";
    public BasketPO() {
        super(REL_URL);
    }

    //отримати список товарів в кошику
    public ArrayList<BasketProduct> getProductsList() {
        ArrayList<BasketProduct> products = new ArrayList<>();
        if (getAllElements(PRODUCT_ITEM).size() > 0) {
            for (int i = 0; i < getAllElements(PRODUCT_ITEM).size(); i++) {
                products.add(new BasketProduct(i + 1));
            }
        } else {
            Reporter.log("В кошику не знайдено жодного товару!!", true);
        }
        return products;
    }
}
