package pages.categoriesOrListingPO.elements;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import utils.Hacks;

import static pages.BasePO.*;

/**
 * Даний клас створений для метода getProductsList() з класу ListingPO. Той метод повертає список товарів в лістингу,
 * а за допомогою методів цього класу можна отримувати інформацію про ті товари зі списку і маніпулювати ними.
 **/

public class ListingProduct {
    private static int index;
    private static String PRODUCT_SEL;

    public ListingProduct(int i) {
        index = i;
        PRODUCT_SEL = "//ul[@appgridlayout]/li[" + index + "]";
    }

    private final String NAME_SEL = "//span[@class=\"goods-tile__title\"]";
    private final String PRICE_SEL = "//span[@class=\"goods-tile__price-value\"]";
    private final String ADD_TO_BASKET_SEL = "//button[@class=\"buy-button goods-tile__buy-button\"]";
    private final String GO_TO_CHECOUT = "//button[@class=\"buy-button goods-tile__buy-button buy-button_state_in-cart\"]";

    //отримати назву товару
    public String getName() {
        return getText(By.xpath(PRODUCT_SEL + NAME_SEL));
    }

    //отримати ціну товару
    public double getPrice() {
        return Hacks.getDouble(getText(By.xpath(PRODUCT_SEL + PRICE_SEL)));
    }

    @Step("Добавляємо товар в кошик")
    public void addToBasket() {
        clickElement(By.xpath(PRODUCT_SEL + ADD_TO_BASKET_SEL));
        waitUntilElementIsClickable(By.xpath(PRODUCT_SEL + GO_TO_CHECOUT));
    }

    @Step("Клікаємо ще раз на товар який добавлений в кошик(переходимо в кошик)")
    public void goToBasket() {
        clickElement(By.xpath(PRODUCT_SEL + GO_TO_CHECOUT));
    }
}
