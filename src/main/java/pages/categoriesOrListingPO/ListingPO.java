package pages.categoriesOrListingPO;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import pages.BasePO;
import pages.categoriesOrListingPO.elements.ListingProduct;

import java.util.ArrayList;
import java.util.Random;

public class ListingPO extends BasePO {
    private static final Random RANDOM = new Random();

    private static final By PRODUCT_ITEM = By.xpath("//div[@class=\"goods-tile__availability goods-tile__availability_type_available\"]/../a/span");
    private static final By AVAILABLE_PRODUCT_ITEM = By.xpath("//div[contains(@class, 'goods-tile__availability_type_limited') or contains(@class, " +
            "'goods-tile__availability_type_available')]/../a/span");

    private static String url;

    public ListingPO() {
        super(url);
    }

    //зайти в лістинг по його CODE і ID
    public ListingPO open(String code, String id) {
        url = "/" + code + "/" + id + "/";
        return (ListingPO) new ListingPO().open();
    }

    //отримати список товарів в лістингу
    public ArrayList<ListingProduct> getProductsList() {
        ArrayList<ListingProduct> products = new ArrayList<>();
        if (countListingProduct() > 0) {
            for (int i = 0; i < getAllElements(PRODUCT_ITEM).size(); i++) {
                products.add(new ListingProduct(i + 1));
            }
        } else {
            Reporter.log("В лістингу не знайдено жодного товару в статусі \"В наявності\"!!", true);
        }
        return products;
    }

    //отримуємо кількість товарів в лістингу
    private int countListingProduct() {
        return getAllElements(PRODUCT_ITEM).size();
    }

    private int countAvailableListingProduct() {
        return getAllElements(AVAILABLE_PRODUCT_ITEM).size();
    }

    //отримуємо рандомний товар в лістингу який є в наявності
    public ListingProduct getRandomListingProduct() {
        int i = 0;
        try {
            i = RANDOM.nextInt(countAvailableListingProduct());
        } catch (IllegalArgumentException e) {
            Assert.fail("В лістингу немає товарів які є в наявності!");
        }
        return getProductsList().get(i);
    }
}
