QA Automation for web applications  
Тестовое задание  
Задача: реализовать автоматизацию следующего E2E сценария:  
Вход в личный кабинет под зарегистрированным аккаунтом на веб сайте https://rozetka.com.ua/. Выбор произвольного товара в каталоге из произвольной категории, добавление товара в корзину.  
Будет плюсом если тесты будут включать репортинг Allure (или любой другой на выбор кандидата).

Стек фреймворка :
Java, Selenium(Selenide), Maven(Gradle), TestNG, Allure

Параметры оценки: 
выполнение поставленной задачи, структурно чистый и понятный код

### Як запустити автотести?
Для запуска автотестів потрібен Maven на ПК.
1) З консолі перейти в папку з автотестами
2) Запустити команду mvn test -Dsurefire.suiteXmlFiles=run_me.xml або з IntellijIDEA

## Знайдені баги: 
https://rozetka.com.ua/genskie-botinki-botiloni/c4634785/

В цьому лістингу не можна добавити товари в кошик. Тому тут тест не буде працювати.
